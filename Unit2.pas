unit Unit2;

interface

uses Winapi.Windows, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Forms,
  Vcl.Controls, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids, Fox_magic,
  Types,IOUtils, JPEG;

type
  TForm2 = class(TForm)
    OKBtn: TButton;
    GroupBox1: TGroupBox;
    Edit1: TEdit;
    Label1: TLabel;
    Memo1: TMemo;
    Counter: TLabel;
    Label8: TLabel;
    Memo2: TMemo;
    Warning: TLabel;
    Timer1: TTimer;
    GroupBox2: TGroupBox;
    Edit2: TEdit;
    Label7: TLabel;
    StringGrid1: TStringGrid;
    StringGrid2: TStringGrid;
    Warning2: TLabel;
    GroupBox3: TGroupBox;
    Memo3: TMemo;
    GroupBox4: TGroupBox;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    RadioButton5: TRadioButton;
    Label2: TLabel;
    Edit3: TEdit;
    procedure Memo2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure UpdateKeywords(Sender: TObject);
    procedure UpdateMats(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
    procedure StringGrid1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure StringGrid2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Memo3Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;
  imagery: array [1..5] of string;

implementation

uses Unit1;

{$R *.dfm}

procedure TForm2.Memo1Change(Sender: TObject);
var charcount: integer;
begin
  if OKBtn.Tag=0 then exit;
  charcount:= Length(StringReplace(StringReplace(Memo1.Lines.Text , #10, '', [rfReplaceAll]), #13, '', [rfReplaceAll]));

  if charcount>250 then
    begin
      form2.Warning.caption:= 'Keywords string too long, please remove '+ inttostr(charcount-250) +' character(s) to proceed.';
      form2.Warning.Visible:= true;
      form2.OKBtn.Enabled:= false;
    end
  else
    begin
      form2.Warning.Visible:= false;
      form2.OKBtn.Enabled:= true;
      item.Title:= form2.Edit1.Text;
      item.Bullets[1]:= form2.StringGrid1.Cells[1,0];
      item.Bullets[2]:= form2.StringGrid1.Cells[1,1];
      item.Bullets[3]:= form2.StringGrid1.Cells[1,2];
      item.Bullets[4]:= form2.StringGrid1.Cells[1,3];
      item.Bullets[5]:= form2.StringGrid1.Cells[1,4];
      item.Keywords:= Memo1.Lines.Text;
    end;
end;

procedure TForm2.UpdateKeywords(Sender: TObject);
var combined_keywords: widestring;
    i: integer;
begin
  if OKBtn.Tag=0 then exit;

  combined_keywords:=
  Edit1.Text + ' ' +
  form2.StringGrid1.Cells[1,0] + ' ' +
  form2.StringGrid1.Cells[1,1] + ' ' +
  form2.StringGrid1.Cells[1,2] + ' ' +
  form2.StringGrid1.Cells[1,3] + ' ' +
  form2.StringGrid1.Cells[1,4];

  for i := 0 to memo2.Lines.Count-1 do
    combined_keywords:= combined_keywords +' '+ form2.Memo2.Lines[i];
  form2.Memo1.Clear;
  form2.Memo1.Lines.Add(combined_keywords);
  form2.Counter.Caption:= 'Final keywords ('+ inttostr(length(combined_keywords)) +')';

  if length(combined_keywords)>250 then
    begin
      form2.Warning.caption:= 'Keywords string too long, please remove '+ inttostr(length(combined_keywords)-250) +' character(s) to proceed.';
      form2.Warning.Visible:= true;
      form2.OKBtn.Enabled:= false;
    end
  else
    begin
      form2.Warning.Visible:= false;
      item.Keywords:= combined_keywords;
      form2.OKBtn.Enabled:= true;
      item.Title:= form2.Edit1.Text;
      item.Bullets[1]:= form2.StringGrid1.Cells[1,0];
      item.Bullets[2]:= form2.StringGrid1.Cells[1,1];
      item.Bullets[3]:= form2.StringGrid1.Cells[1,2];
      item.Bullets[4]:= form2.StringGrid1.Cells[1,3];
      item.Bullets[5]:= form2.StringGrid1.Cells[1,4];
    end;
end;

procedure TForm2.UpdateMats(Sender: TObject);
var valid:boolean;
begin
  if OKBtn.Tag=0 then exit;

  valid:= true;
  if not isNumerical(form2.StringGrid2.Cells[1,5]) then valid:= false;
  if not isNumerical(form2.StringGrid2.Cells[1,6]) then valid:= false;
  if not isNumerical(form2.StringGrid2.Cells[1,7]) then valid:= false;
  if not isNumerical(form2.StringGrid2.Cells[1,8]) then valid:= false;

  if not valid then
    begin
      form2.Warning2.Visible:= true;
      form2.OKBtn.Enabled:= false;
    end
  else
    begin
      form2.Warning2.Visible:= false;
      form2.OKBtn.Enabled:= true;
      Item.FabricString:= form2.Edit2.Text;
      Item.Fabrics[1]:= form2.StringGrid2.Cells[1,0];
      Item.Fabrics[2]:= form2.StringGrid2.Cells[1,1];
      Item.Fabrics[3]:= form2.StringGrid2.Cells[1,2];
      Item.Fabrics[4]:= form2.StringGrid2.Cells[1,3];
      Item.Fabrics[5]:= form2.StringGrid2.Cells[1,4];
      Item.Weight:=     form2.StringGrid2.Cells[1,5];
      Item.Dimensions[1]:= form2.StringGrid2.Cells[1,6];
      Item.Dimensions[2]:= form2.StringGrid2.Cells[1,7];
      Item.Dimensions[3]:= form2.StringGrid2.Cells[1,8];
    end;
end;

procedure TForm2.Edit2Change(Sender: TObject);
begin
  UpdateMats(self);
end;

procedure LoadImages;
var work: TStringDynArray;
    i: integer;
begin
  for i := 1 to Length(work) do
    imagery[i]:= '';

  try
    work:= TDirectory.GetFiles(GetCurrentDir+'\Mercury_data\Export\'+Item.Title,'*.jpeg',TSearchOption.soTopDirectoryOnly);
    if Length(work)=0 then
      form2.Edit3.Text:= 'ERROR: No images found'
    else
      begin
        for i := 0 to Length(work) do
          begin
            if i>5 then break;
            if FileExists(work[i]) then
              begin
                imagery[i+1]:= work[i];
              end;
          end;
          {
          if FileSize(work[i])>largest then
            begin
              largest:= FileSize(work[i]);
              result:= work[i];
            end;
            }
      end;
  except
    on E: Exception do
      begin
          Log('Error happened while getting file sizes of the images. Couldn''t determine the Main image.');
      end;
  end;

  if imagery[1]<>'' then
    form2.Image1.Picture.LoadFromFile(imagery[1]);
  if imagery[2]<>'' then
    form2.Image2.Picture.LoadFromFile(imagery[2]);
  if imagery[3]<>'' then
    form2.Image3.Picture.LoadFromFile(imagery[3]);
  if imagery[4]<>'' then
    form2.Image4.Picture.LoadFromFile(imagery[4]);
  if imagery[5]<>'' then
    form2.Image5.Picture.LoadFromFile(imagery[5]);

  form2.RadioButton1.Checked:= true;
  form2.Edit3.Text:= stringreplace(imagery[1],GetCurrentDir+'\Mercury_data\Export\'+Item.Title+'\','',[rfReplaceAll, rfIgnoreCase]);
  //Item.MainImage:= FindLargestFile(GetCurrentDir+'\Mercury_data\Export\'+Item.Title,'*.jpeg');
end;

procedure TForm2.FormShow(Sender: TObject);
var i:integer;
begin
  OKBtn.Tag:=0;
  for i := 0 to form2.StringGrid1.RowCount-1 do
    StringGrid1.Cells[1,i]:='';
  for i := 0 to form2.StringGrid2.RowCount-1 do
    StringGrid2.Cells[1,i]:='';
  memo1.Clear;
  memo2.Clear;
  memo3.Clear;

  form2.Edit1.Text:= Item.Title;
  form2.Edit2.Text:= Item.FabricString;
  memo3.Lines.Add(Item.Description);

  form2.StringGrid1.Cells[0,0]:= 'Bullet1';
  form2.StringGrid1.Cells[0,1]:= 'Bullet2';
  form2.StringGrid1.Cells[0,2]:= 'Bullet3';
  form2.StringGrid1.Cells[0,3]:= 'Bullet4';
  form2.StringGrid1.Cells[0,4]:= 'Bullet5';
  form2.StringGrid1.Cells[1,0]:= Item.Bullets[1];
  form2.StringGrid1.Cells[1,1]:= Item.Bullets[2];
  form2.StringGrid1.Cells[1,2]:= Item.Bullets[3];
  form2.StringGrid1.Cells[1,3]:= Item.Bullets[4];
  form2.StringGrid1.Cells[1,4]:= Item.Bullets[5];
  form2.StringGrid1.ColWidths[0]:= 50;
  form2.StringGrid1.ColWidths[1]:= form2.StringGrid1.Width-50;

  form2.StringGrid2.Cells[0,0]:= 'Fabric1';
  form2.StringGrid2.Cells[0,1]:= 'Fabric2';
  form2.StringGrid2.Cells[0,2]:= 'Fabric3';
  form2.StringGrid2.Cells[0,3]:= 'Fabric4';
  form2.StringGrid2.Cells[0,4]:= 'Fabric5';
  form2.StringGrid2.Cells[0,5]:= 'Weight';
  form2.StringGrid2.Cells[0,6]:= 'Length';
  form2.StringGrid2.Cells[0,7]:= 'Width';
  form2.StringGrid2.Cells[0,8]:= 'Height';
  form2.StringGrid2.Cells[1,0]:= Item.Fabrics[1];
  form2.StringGrid2.Cells[1,1]:= Item.Fabrics[2];
  form2.StringGrid2.Cells[1,2]:= Item.Fabrics[3];
  form2.StringGrid2.Cells[1,3]:= Item.Fabrics[4];
  form2.StringGrid2.Cells[1,4]:= Item.Fabrics[5];
  form2.StringGrid2.Cells[1,5]:= Item.Weight;
  form2.StringGrid2.Cells[1,6]:= Item.Dimensions[1];
  form2.StringGrid2.Cells[1,7]:= Item.Dimensions[2];
  form2.StringGrid2.Cells[1,8]:= Item.Dimensions[3];
  form2.StringGrid2.ColWidths[0]:= 50;
  form2.StringGrid2.ColWidths[1]:= form2.StringGrid2.Width-50;

  timer1.Enabled:= true;
end;

procedure TForm2.Memo2Click(Sender: TObject);
begin
  if memo2.Lines[0]='Input additional keywords here.' then
    memo2.Clear;
end;

procedure TForm2.Memo3Change(Sender: TObject);
var tmp: string;
    i: integer;
begin
  for i := 0 to memo3.Lines.Count-1 do
    tmp:= tmp +' '+ memo3.Lines[i];
  item.Description:= tmp;
end;

procedure TForm2.RadioButton1Click(Sender: TObject);
begin
  form2.Edit3.Text:= stringreplace(imagery[(Sender as TRadioButton).Tag],GetCurrentDir+'\Mercury_data\Export\'+Item.Title+'\','',[rfReplaceAll, rfIgnoreCase]);
  Item.MainImage:= form2.Edit3.Text;
end;

procedure TForm2.StringGrid1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  UpdateKeywords(self);
end;

procedure TForm2.StringGrid2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  UpdateMats(self);
end;

procedure TForm2.Timer1Timer(Sender: TObject);
begin
  timer1.Enabled:= false;
  OKBtn.Tag:=1;
  UpdateKeywords(self);
  LoadImages;
end;

end.
