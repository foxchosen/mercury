object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Mercury v2.0'
  ClientHeight = 672
  ClientWidth = 1263
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object logs: TMemo
    Left = 0
    Top = 512
    Width = 1263
    Height = 143
    Align = alBottom
    Lines.Strings = (
      'Application started.')
    ScrollBars = ssVertical
    TabOrder = 0
    WordWrap = False
  end
  object CEFWindowParent1: TCEFWindowParent
    Left = 0
    Top = 41
    Width = 1263
    Height = 471
    Align = alClient
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1263
    Height = 41
    Align = alTop
    TabOrder = 2
    object Edit1: TEdit
      AlignWithMargins = True
      Left = 340
      Top = 9
      Width = 919
      Height = 23
      Margins.Top = 8
      Margins.Bottom = 8
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Text = 
        'https://www.walmart.com/ip/Danskin-Now-Women-s-Active-Bungee-Tan' +
        'k-with-Mesh-Detail/55089733'
      OnClick = Edit1Click
      ExplicitHeight = 21
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 336
      Height = 39
      Align = alLeft
      TabOrder = 1
      object Button1: TButton
        Left = 104
        Top = 9
        Width = 60
        Height = 25
        Caption = 'Load URL'
        Enabled = False
        TabOrder = 0
        OnClick = Button1Click
      end
      object Button2: TButton
        Left = 8
        Top = 8
        Width = 90
        Height = 25
        Caption = 'Use Clipboard'
        Default = True
        Enabled = False
        TabOrder = 1
        OnClick = Button2Click
      end
      object Button3: TButton
        Left = 256
        Top = 9
        Width = 73
        Height = 25
        Caption = 'open Excel'
        Enabled = False
        TabOrder = 2
        OnClick = Button3Click
      end
    end
  end
  object CheckBox1: TCheckBox
    Left = 171
    Top = 8
    Width = 80
    Height = 17
    Hint = 'Will slow down processing, but allow for navigation'
    Caption = 'Use browser'
    TabOrder = 3
  end
  object ProgressBar1: TProgressBar
    Left = 0
    Top = 655
    Width = 1263
    Height = 17
    Align = alBottom
    TabOrder = 4
  end
  object StringGrid1: TStringGrid
    Left = 0
    Top = 41
    Width = 1263
    Height = 471
    Align = alClient
    ColCount = 118
    FixedCols = 0
    FixedRows = 4
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
    TabOrder = 5
    Visible = False
  end
  object IdHTTP1: TIdHTTP
    OnStatus = IdHTTP1Status
    IOHandler = OpenSSL1
    AllowCookies = True
    HandleRedirects = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    Request.BasicAuthentication = False
    Request.UserAgent = 
      'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:56.0) G' +
      'ecko/20100101 Firefox/56.0'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    CookieManager = IdCookieManager1
    Left = 24
    Top = 48
  end
  object IdAntiFreeze1: TIdAntiFreeze
    Left = 24
    Top = 96
  end
  object IdCookieManager1: TIdCookieManager
    Left = 24
    Top = 144
  end
  object OpenSSL1: TIdSSLIOHandlerSocketOpenSSL
    MaxLineAction = maException
    Port = 0
    DefaultPort = 0
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 24
    Top = 192
  end
  object Chromium1: TChromium
    OnTitleChange = Chromium1TitleChange
    Left = 112
    Top = 48
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 112
    Top = 96
  end
end
