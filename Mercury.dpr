program Mercury;

{$I cef.inc}

uses
  Vcl.Forms,
  Unit1 in 'Unit1.pas' {Form1},
  XSuperJSON in '..\_Shared\XSuperJSON.pas',
  XSuperObject in '..\_Shared\XSuperObject.pas',
  Fox_magic in '..\_Shared\Fox_magic.pas',
  uCEFApplication,
  Unit2 in 'Unit2.pas' {Form2};

{$R *.res}
// {$SetPEFlags IMAGE_FILE_LARGE_ADDRESS_AWARE}

begin
  GlobalCEFApp                      := TCefApplication.Create;
  GlobalCEFApp.FrameworkDirPath     := 'cef';
  GlobalCEFApp.ResourcesDirPath     := 'cef';
  GlobalCEFApp.LocalesDirPath       := 'cef\locales';
  GlobalCEFApp.cache                := 'cef\cache';
  GlobalCEFApp.cookies              := 'cef\cookies';
  GlobalCEFApp.UserDataPath         := 'cef\User Data';

  if GlobalCEFApp.StartMainProcess then
    begin
//      ReportMemoryLeaksOnShutdown := DebugHook <> 0;
      Application.Initialize;
      Application.MainFormOnTaskbar := True;
      Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.Run;
    end;

  GlobalCEFApp.Free;
end.
