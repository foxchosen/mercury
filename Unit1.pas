unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, IdAntiFreezeBase,
  IdAntiFreeze, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdHTTP, Fox_magic, IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL,
  IdSSLOpenSSL, IdCookieManager, StrUtils, ClipBrd, JSON, XSuperObject, XSuperJSON,
  ShellApi, Vcl.ExtCtrls, uCEFWindowParent, uCEFChromium, uCEFInterfaces,
  uCEFApplication, uCEFTypes, uCEFConstants, System.UITypes, inifiles,
  Vcl.ComCtrls, Vcl.Grids, IOUtils;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Edit1: TEdit;
    IdHTTP1: TIdHTTP;
    IdAntiFreeze1: TIdAntiFreeze;
    logs: TMemo;
    IdCookieManager1: TIdCookieManager;
    OpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    Button2: TButton;
    Chromium1: TChromium;
    CEFWindowParent1: TCEFWindowParent;
    Panel1: TPanel;
    Panel2: TPanel;
    CheckBox1: TCheckBox;
    Timer1: TTimer;
    Button3: TButton;
    ProgressBar1: TProgressBar;
    StringGrid1: TStringGrid;
    procedure Button1Click(Sender: TObject);
    procedure IdHTTP1Status(ASender: TObject; const AStatus: TIdStatus;
      const AStatusText: string);
    procedure Edit1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Chromium1TitleChange(Sender: TObject; const browser: ICefBrowser;
      const title: ustring);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

type fItem = class
      ID: string;
      Title: string;
      Brand: string;
      Price: extended;
      Dept: string;
      Gender: string;
      Description: string;
      BulletPointsString: string;
      Bullets: array [1..5] of string;
      FabricString: string;
      Fabrics: array [1..5] of string;
      DimensionsString: string;
      Dimensions: array [1..3] of string;
      Weight: string;
      MainImage:string;
      Category: string;
      Feed: string;
      Keywords: string;
end;

type fVariant = class
      Color,ColorCode,Color_primary: string;
      Size,Size_map: string;
      UPC: string;
      Image1, Image2: string;
end;

var
  Form1: TForm1;
  Item: fItem;
  images,primary_colours: TStringList;
  big_colours,big_sizes,big_categories,big_feeds: TStringList;
  UPCs: array of string;
  vars: array of fVariant;
  parentID: integer;
  lastFile,nextUPC: string;

const local: boolean = false;

implementation

{$R *.dfm}

uses Unit2;

function DictionariesInit:boolean; forward;

function Compare(const one,two: string): Integer;
begin
  Result := CompareText(one,two);
end;

procedure Swap(var one,two: string);
var
  temp: string;
begin
  temp := one;
  one := two;
  two := temp;
end;

procedure Sort;
var
  i, n: Integer;
  Swapped: Boolean;
begin
  n := Length(UPCs);
  repeat
    Swapped := False;
    for i := 1 to n-1 do begin
      if Compare(UPCs[i-1], UPCs[i])>0 then begin
        Swap(UPCs[i-1], UPCs[i]);
        Swapped := True;
      end;
    end;
    dec(n);
  until not Swapped;
end;

function FieldExists(const item1: IMember; const AField: String): Boolean;
var item2: IMember;
begin
  result:=false;
  for item2 in item1.AsObject do
    if item2.Name=AField then
      result:=true;
end;

function GetImage(const JSONObj: ISuperObject; const item1: String): string;
begin
  result:='ERROR';
  result:= JSONObj['product.images."'+item1+'".assetSizeUrls.zoom'].ToString;
  if result='null' then
    result:= JSONObj['product.images."'+item1+'".assetSizeUrls.main'].ToString;
  result:= ExtractData(result,'*start*','?');
  result:= stringreplace(result,'?','"',[rfReplaceAll, rfIgnoreCase]);
  result:= RemoveQuotes(result);
  images.Add(result);
  result:= ExtractData(result,'asr/','*end*');
end;

procedure IncreaseParentID;
var appINI : TIniFile;
begin
  inc(parentID);
  appINI := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini')) ;
  try
     appINI.WriteInteger('parentID','wal',parentID);
  finally
     appINI.Free;
  end;
end;

procedure IncreaseUPC;
var appINI : TIniFile;
    numero: int64;
begin
  numero:= StrToInt64(nextUPC);
  inc(numero);
  nextUPC:= IntToStr(numero);

  appINI := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini')) ;
  try
     appINI.WriteString('nextUPC' ,'UPC',nextUPC);
  finally
     appINI.Free;
  end;
end;

procedure ReadINI;
var appINI : TIniFile;
begin
  appINI := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini')) ;
  try
     parentID:= appINI.ReadInteger('parentID','wal',100001);
     nextUPC:=  appINI.ReadString ('nextUPC' ,'UPC','');
  finally
     appINI.Free;
  end;

  if parentID=100001 then
    messagedlg('First start - setting default value: parentID=wal100001',mtInformation, [mbOK], 0);
  if nextUPC='' then
    begin
      nextUPC:='067861530500';
      messagedlg('First start - setting default value: UPC=067861530500',mtInformation, [mbOK], 0);
    end;
  if not isNumerical(nextUPC) then
    messagedlg('nextUPC value corrupted (not a number), reverting to default value: UPC=067861530500',mtInformation, [mbOK], 0);
end;

function MatchDepartment(input,gender:string):string;
var guess:boolean;
begin
{
baby-boys
baby-girls
boys
girls
mens
unisex-baby
womens
}

  input:= lowercase(input);
  if gender='' then
    begin
      Log('No gender specified, guessing category by keywords');
      guess:= true;
    end
  else guess:=false;

  if ContainsText(input,'men') or (gender='Men')  then
    result:= 'mens';
  if ContainsText(input,'women') or (gender='Women') or containstext(input,'maternity') then
    result:= 'womens';

  if ContainsText(input,'junior') then
    begin
      if gender='Men' then
        result:= 'boys';
      if gender='Women' then
        result:= 'girls';
      if result='' then
        result:= 'No gender set in specifications but item is for juniors. Couldn''t determine category.';
    end;

  if (result<>'') then
    begin
      if guess then
        Log('Category guessed from keywords in item name: '+result)
    end
  else
    begin
      result:= inputBox('Enter category manually for this item ("womens" etc.)',input,'');
      Log('Category entered manually: '+result)
    end;
end;

procedure RestoreDesktop(cursor:TCursor);
begin
  form1.button1.Enabled:= true;
  form1.button2.Enabled:= true;
  Screen.Cursor:= cursor;
end;

procedure DecompileDimensions;
var line:string;
    pointer:integer;
begin
// '51.00 x 33.00 x 1.12 Inches'
  line:= item.DimensionsString;
  if pos('x',line)=0 then exit;

  pointer:= 0;
  repeat
    inc(pointer);
    item.Dimensions[pointer]:= trim(ExtractData(line,'*start*',' '));
    delete(line,1,pos('x',line)+1);
  until pointer=3;
end;

procedure DecompileFabrics;
var tmp,add,line:string;
    n,pointer:integer;
    i: Integer;
    alreadySaved: boolean;
begin
// 'Body: 100% Polyester Interlock, 130 Gsm Mesh: 95% Polyester 5% Spandex Mesh, 110 Gsm'

  if pos('%',item.FabricString)=0 then
    for n := 1 to 5 do
      if pos('%',item.Bullets[n])>0 then
        begin
          Log('Fabrics not listed in Specs, but found in bullet points. Parsing values.');
          item.FabricString:= item.Bullets[n];
        end;

  line:= item.FabricString;
  if line='' then exit;
  if pos('%',line)=0 then
    begin
      item.Fabrics[1]:= line;
      exit;
    end;

  line:= stringreplace(line,';',' ',[rfReplaceAll, rfIgnoreCase]);
  pointer:=0;

  while pos('% ',line)>0 do
    begin
      inc(pointer);
      n:= pos('%',line);
      delete(line,1,n+1);
      add:= ExtractData(line,'*start*',' ');
      if add='' then continue;
      alreadySaved:= false;
      for i := 1 to 5 do
        if item.Fabrics[i]=add then alreadySaved:= true;
      if not alreadySaved then
        item.Fabrics[pointer]:= stringreplace(add,',',' ',[rfReplaceAll]);
    end;

  if tmp<>'' then
    item.Fabrics[5]:= tmp;
end;

function RemoveLeadingZeros(line:string):string;
var i:integer;
begin
  for i := 1 to length(line) do
    if not(line[i]='0') then
      begin
        result:= copy(line,i,20);
        exit;
      end;
end;

function WUPCtoUPC(line:string):string;
{UPC CHECK DIGIT CALC
Add the digits in the odd-numbered positions (first, third, fifth, etc.) together and multiply by three.
Add the digits (up to but not including the check digit) in the even-numbered positions (second, fourth, sixth, etc.) to the result.
Take the remainder of the result divided by 10 (modulo operation) and if not 0, subtract this from 10 to derive the check digit.
For instance, the UPC-A barcode for a box of tissues is "036000241457". The last digit is the check digit "7", and if the other numbers are correct then the check digit calculation must produce 7.

Add the odd number digits: 0+6+0+2+1+5 = 14.
Multiply the result by 3: 14 � 3 = 42.
Add the even number digits: 3+0+0+4+4 = 11.
Add the two results together: 42 + 11 = 53.
To calculate the check digit, take the remainder of (53 / 10), which is also known as (53 modulo 10), and if not 0, subtract from 10. Therefore, the check digit value is 7. i.e. (53 / 10) = 5 remainder 3; 10 - 3 = 7.}
var i,odd,even,check:integer;
begin
  if line='xxx' then
    line := inputbox('WUPC -> UPC convertor', 'Insert WUPC number', '0070330460078');

  line:= RemoveLeadingZeros(line);
  odd:=  0;
  even:= 0;

  for i := 1 to length(line) do
    begin
      if (i mod 2= 0) then
        even:= even + strtoint(line[i])
      else
        odd:=  odd  + strtoint(line[i]);
    end;

  odd:= odd * 3;
  check:= (odd + even) mod 10;

  if check=0 then
    result:= line + '0'
  else
    begin
      check:= 10- ((odd + even) mod 10);
      result:= line + inttostr(check);
    end;

  while length(result)<13 do
    result:= '0' + result;

  //Log('WUPC '+line+' -> UPC '+RemoveLeadingZeros(result));
end;

procedure Progress(value:integer);
begin
  form1.ProgressBar1.Position:= value;
  form1.Update;
end;

function ColorToPrimary(input:string):string;
var i:integer;
    reply:string;
    valid:boolean;
begin
  result:= '';
  if input='' then exit;


  for i := 0 to Primary_colours.Count-1 do
    if ContainsText(input,Primary_colours[i]) then
      begin
        result:= Primary_colours[i];
        exit;
      end;

  for i := 0 to big_colours.Count-1 do
    if ContainsText(big_colours[i],input) then
      begin
        result:= ExtractData(big_colours[i],'*start*',',');
        delete(result,length(result),1);
        exit;
      end;

  Log('No match for this colour, opening dictionary for new entry');
  repeat
    valid:= false;
    reply:= Inputbox('Assign primary colour for this entry (eg. "Pink")',input,'');

    if reply<>'' then
    begin
      reply[1]:= UpCase(reply[1]);
      for i := 0 to Primary_colours.Count-1 do
        if reply= Primary_colours[i] then
          begin
            result:= Primary_colours[i];
            big_colours.Add(reply+','+input);
            big_colours.SaveToFile('Mercury_data\colours.dict');
            Log('New colour match assigned: ' +input+ '='+ reply);
            valid:= true;
          end;
      if not valid then
        ShowMessage('The colour you specified is not one of the primary colours.');
    end;

    if reply='' then
        begin
          valid:= true;
          Log('Cancelled adding new colour pair to dictionary.');
        end;
  until valid;

  result:= stringreplace(result,',','',[rfReplaceAll, rfIgnoreCase]);
end;

function SizeMapping(input:string;amz:boolean):string;
var i:integer;
    reply,reply2:string;
    valid:boolean;
begin
  result:= '';
// Pretty Size,	Amazon Size,	Size Map
// big_sizes.Add('XXS,XX-Small,XX-Small');

  input:= stringreplace(input,'x', 'X',[rfReplaceAll, rfIgnoreCase]);
  input:= stringreplace(input,' X','X',[rfReplaceAll, rfIgnoreCase]);
  input:= stringreplace(input,'X ','X',[rfReplaceAll, rfIgnoreCase]);

  for i := 0 to big_sizes.Count-1 do
    if ExtractValue(big_sizes[i],0)=input then
      begin
        if amz then
          result:= ExtractValue(big_sizes[i],1)
        else
          result:= ExtractValue(big_sizes[i],2);
        exit;
      end;

  if result='' then
  repeat
    valid:= false;
    reply:= inputBox('No match, please input AMZ size',input + ' = ','');
    reply2:=inputBox('No match, please input size map',input + ' = ','');

    if (reply<>'') and (reply2<>'') then
    begin
      if amz then
        result:= reply
      else
        result:= reply2;

      big_sizes.Add(input+','+reply+','+reply2);
      big_sizes.SaveToFile('Mercury_data\sizes.dict');
      Log('New size added to dictionary = '+result);
      valid:= true;
    end;

    if reply='' then
        begin
          valid:= true;
          Log('Cancelled adding new sizes data to dictionary.');
        end;
  until valid;

  result:= stringreplace(result,',','',[rfReplaceAll, rfIgnoreCase]);
end;

function AssignCategory(title:string):string;
var i,j: integer;
    keywords: TStringList;
    matchedWord: string;
begin
  result:='';
  matchedWord:='';
  title:= ' '+title+' ';
  keywords:= TStringList.Create;
  keywords.Delimiter:= ',';
  keywords.StrictDelimiter:= true;

  for i := 0 to big_categories.Count-1 do
    begin
      keywords.DelimitedText:= ExtractData(big_categories[i],'"','*end*');
      keywords[keywords.Count-1]:= removeQuotes(keywords[keywords.Count-1]);

      for j := 0 to keywords.Count-1 do
        if ContainsText(title,' '+trim(keywords[j])+' ') then
          if length(keywords[j])>length(matchedWord) then
            begin
              matchedWord:= keywords[j];
              result:= ExtractData(big_categories[i],'*start*',',');
              delete(result,length(result),1);
            end;
    end;
  keywords.Free;

  if result='' then
    begin
      result:= inputBox('No match. Enter category manually ("jeans" etc.)',title,'');
      Log('Couldn''t find any matching keywords, category entered manually as '+result);
    end;
end;

function AssignFeed(cat:string):string;
var i: integer;
    pair: TStringList;
begin
  result:='';
  pair:= TStringList.Create;
  pair.Delimiter:= ',';
  pair.StrictDelimiter:= true;

  for i := 0 to big_feeds.Count-1 do
    begin
      pair.DelimitedText:= big_feeds[i];
      if pair[0]=cat then
        begin
          result:= pair[1];
          break;
        end;
    end;
  pair.Free;

  if result='' then
    begin
      result:= inputBox('No match. Enter feed manually ("pants" etc.)',cat,'');
      Log('Couldn''t find any matching keywords, feed entered manually as '+result);
    end;
end;

procedure GridHeaders;
var i,j: integer;
begin
  for i := 3 to form1.StringGrid1.RowCount do
    for j := 0 to form1.StringGrid1.ColCount do
      form1.StringGrid1.Cells[j,i]:= '';

  form1.StringGrid1.Cells[0,0]:= 'TemplateType=fptcustom';
  form1.StringGrid1.Cells[1,0]:= 'Version=2017.1212';
  form1.StringGrid1.Cells[2,0]:= 'TemplateSignature=UEFOVFM=';
  form1.StringGrid1.Cells[3,0]:= 'The top 3 rows are for Amazon.com use only. Do not modify or delete the top 3 rows.';
  form1.StringGrid1.Cells[0,1]:= 'Product Type';
  form1.StringGrid1.Cells[1,1]:= 'Seller SKU';
  form1.StringGrid1.Cells[2,1]:= 'Product ID';
  form1.StringGrid1.Cells[3,1]:= 'Product ID Type';
  form1.StringGrid1.Cells[4,1]:= 'Brand Name';
  form1.StringGrid1.Cells[5,1]:= 'Product Name';
  form1.StringGrid1.Cells[6,1]:= 'Color';
  form1.StringGrid1.Cells[7,1]:= 'Color Map';
  form1.StringGrid1.Cells[8,1]:= 'Department';
  form1.StringGrid1.Cells[9,1]:= 'Size';
  form1.StringGrid1.Cells[10,1]:= 'Size Map';
  form1.StringGrid1.Cells[11,1]:= '...';
  form1.StringGrid1.Cells[0,2]:= 'feed_product_type';
  form1.StringGrid1.Cells[1,2]:= 'item_sku';
  form1.StringGrid1.Cells[2,2]:= '...';

  form1.StringGrid1.Visible:= true;
  form1.CEFWindowParent1.Visible:=false;
end;

procedure GridOutput;
var i,line,offset: integer;
begin
  // 116 columns total
  offset:= 4;
  line:=2;
  for i := -1 to length(vars)-1 do
    begin
      inc(line);
      if i<0 then
        begin
          form1.StringGrid1.Cells[1,line]:= 'wal'+inttostr(parentID);           // shell
          form1.StringGrid1.Cells[2,line]:= '';
          form1.StringGrid1.Cells[3,line]:= '';
          form1.StringGrid1.Cells[5,line]:= AddQuotes(item.Title);
          form1.StringGrid1.Cells[30,line]:= 'http://www.allstarselling.com/images/'+item.MainImage;
          form1.StringGrid1.Cells[37,line]:= 'Parent';
        end
      else
        begin
          form1.StringGrid1.Cells[1,line]:= vars[line-offset].UPC;              // variants
          form1.StringGrid1.Cells[2,line]:= vars[line-offset].UPC;
          form1.StringGrid1.Cells[3,line]:= 'UPC';
          form1.StringGrid1.Cells[5,line]:= AddQuotes(item.Title + ' ('+vars[line-offset].Size+', '+vars[line-offset].Color+')');
          form1.StringGrid1.Cells[6,line]:= vars[line-offset].Color;
          form1.StringGrid1.Cells[7,line]:= vars[line-offset].Color_primary;
          form1.StringGrid1.Cells[9,line]:=  AddQuotes(SizeMapping(vars[line-offset].Size,true));
          form1.StringGrid1.Cells[10,line]:= AddQuotes(SizeMapping(vars[line-offset].Size,false));
          form1.StringGrid1.Cells[30,line]:= Addquotes('http://www.allstarselling.com/images/'+vars[line-offset].Image1);

          if vars[line-offset].Image2<>'' then
          form1.StringGrid1.Cells[31,line]:= Addquotes('http://www.allstarselling.com/images/'+vars[line-offset].Image2);

          form1.StringGrid1.Cells[37,line]:= 'Child';
          form1.StringGrid1.Cells[38,line]:= 'wal'+inttostr(parentID);
          form1.StringGrid1.Cells[39,line]:= 'Variation';
        end;

      form1.StringGrid1.Cells[0,line]:= item.Feed;                              // list for all
      form1.StringGrid1.Cells[4,line]:= AddQuotes(item.Brand);
      form1.StringGrid1.Cells[8,line]:= item.Dept;
      form1.StringGrid1.Cells[11,line]:= 'FALSE';
      form1.StringGrid1.Cells[12,line]:= item.Fabrics[1];
      form1.StringGrid1.Cells[13,line]:= item.Fabrics[2];
      form1.StringGrid1.Cells[14,line]:= item.Fabrics[3];
      form1.StringGrid1.Cells[15,line]:= item.Fabrics[4];
      form1.StringGrid1.Cells[16,line]:= item.Fabrics[5];
      form1.StringGrid1.Cells[17,line]:= AddQuotes(item.FabricString);
      form1.StringGrid1.Cells[27,line]:= floattostr(item.Price+10);
      form1.StringGrid1.Cells[28,line]:= '0';
      form1.StringGrid1.Cells[40,line]:= 'sizecolor';
      form1.StringGrid1.Cells[41,line]:= Item.Category;
      form1.StringGrid1.Cells[42,line]:= AddQuotes(item.Description);
      form1.StringGrid1.Cells[47,line]:= AddQuotes(item.Bullets[1]);
      form1.StringGrid1.Cells[48,line]:= AddQuotes(item.Bullets[2]);
      form1.StringGrid1.Cells[49,line]:= AddQuotes(item.Bullets[3]);
      form1.StringGrid1.Cells[50,line]:= AddQuotes(item.Bullets[4]);
      form1.StringGrid1.Cells[51,line]:= AddQuotes(item.Bullets[5]);
      form1.StringGrid1.Cells[52,line]:= AddQuotes(item.Keywords);
      form1.StringGrid1.Cells[61,line]:= Item.Weight;
      form1.StringGrid1.Cells[62,line]:= 'OZ';
      form1.StringGrid1.Cells[63,line]:= 'OZ';
      form1.StringGrid1.Cells[64,line]:= Item.Weight;
      form1.StringGrid1.Cells[65,line]:= Item.Dimensions[1];
      form1.StringGrid1.Cells[66,line]:= Item.Dimensions[2];
      form1.StringGrid1.Cells[67,line]:= Item.Dimensions[3];
      form1.StringGrid1.Cells[70,line]:= 'IN';
      form1.StringGrid1.Cells[72,line]:= Item.Dimensions[1];
      form1.StringGrid1.Cells[73,line]:= Item.Dimensions[2];
      form1.StringGrid1.Cells[74,line]:= Item.Dimensions[3];
      form1.StringGrid1.Cells[75,line]:= Item.Weight;
      form1.StringGrid1.Cells[76,line]:= 'OZ';
      form1.StringGrid1.Cells[77,line]:= 'IN';
    end;
end;

function GridExport_TAB:string;
var i,j:integer;
    exported: TStringList;
    line: string;
begin
  exported:= TStringList.Create;
  exported.Add('TemplateType=fptcustom'+#9+'Version=2017.1212'+#9+'TemplateSignature=UEFOVFM='+#9+'The top 3 rows are for Amazon.com use only. Do not modify or delete the top 3 rows.'+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+'Images'+#9+''+#9+''+#9+''+#9+''+#9+''+#9+'Variation'+#9+''+#9+''+#9+''+#9+'Basic'+#9+''+#9+''+#9+''+#9+''+#9+''+#9+'Discovery'+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+'Ungrouped'+#9+''+#9+''+#9+''+#9+'Dimensio'+
'ns'+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+'Fulfillment'+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+'Compliance'+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+'Offer'+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+''+#9+'');
  exported.Add('Product Type'+#9+'Seller SKU'+#9+'Product ID'+#9+'Product ID Type'+#9+'Brand Name'+#9+'Product Name'+#9+'Color'+#9+'Color Map'+#9+'Department'+#9+'Size'+#9+'Size Map'+#9+'Is Adult Product'+#9+'Outer Material Type'+#9+'Outer Material Type'+#9+'Outer Material Type'+#9+'Outer Material Type'+#9+'Outer Material Type'+#9+'Material Composition'+#9+'M'+
'aterial Composition'+#9+'Material Composition'+#9+'Material Composition'+#9+'Material Composition'+#9+'Material Composition'+#9+'Material Composition'+#9+'Material Composition'+#9+'Material Composition'+#9+'Material Composition'+#9+'Standard Price'+#9+'Quantity'+#9+'Shipping-Template'+#9+'Main Image URL'+#9+'Other Imag'+
'e URL'+#9+'Other Image URL'+#9+'Other Image URL'+#9+'Swatch Image URL'+#9+'Main Offer Image'+#9+'Offer Image'+#9+'Parentage'+#9+'Parent SKU'+#9+'Relationship Type'+#9+'Variation Theme'+#9+'Item Type Keyword'+#9+'Product Description'+#9+'Style Number'+#9+'Update Delete'+#9+'Manufacturer Part Number'+#9+'Manufacturer'+#9+'Key Product F'+
'eatures'+#9+'Key Product Features'+#9+'Key Product Features'+#9+'Key Product Features'+#9+'Key Product Features'+#9+'Search Terms'+#9+'Search Terms'+#9+'Search Terms'+#9+'Search Terms'+#9+'Search Terms'+#9+'Rise Height'+#9+'Rise Height Unit Of Measure'+#9+'Style'+#9+'material_type'+#9+'Shipping Weight'+#9+'Website Shipping Weigh'+
't Unit Of Measure'+#9+'Item Weight Unit Of Measure'+#9+'Item Weight'+#9+'Item Length'+#9+'Item Width'+#9+'Item Height'+#9+'item_volume'+#9+'Item Volume Unit Of Measure'+#9+'Item Dimensions Unit Of Measure'+#9+'Fulfillment Center ID'+#9+'Package Height'+#9+'Package Width'+#9+'Package Length'+#9+'Package Weight'+#9+'Package W'+
'eight Unit Of Measure'+#9+'Package Dimensions Unit Of Measure'+#9+'Cpsia Warning'+#9+'CPSIA Warning Description'+#9+'Fabric Type'+#9+'Import Designation'+#9+'Country of Publication'+#9+'Consumer Notice'+#9+'batteries_required'+#9+'are_batteries_included'+#9+'Battery Type'+#9+'Number of Batteries Required'+#9+'numb'+
'er_of_lithium_metal_cells'+#9+'number_of_lithium_ion_cells'+#9+'lithium_battery_packaging'+#9+'lithium_battery_energy_content'+#9+'lithium_battery_energy_content_unit_of_measure'+#9+'lithium_battery_weight'+#9+'lithium_battery_weight_unit_of_measure'+#9+'Manufacturer''s Suggested Retail Pri'+
'ce'+#9+'Item Condition'+#9+'Condition Note'+#9+'Product Tax Code'+#9+'Handling Time'+#9+'Launch Date'+#9+'Release Date'+#9+'Restock Date'+#9+'Sale Price'+#9+'Sale Start Date'+#9+'Sale End Date'+#9+'Stop Selling Date'+#9+'Max Aggregate Ship Quantity'+#9+'Package Quantity'+#9+'Number of Items'+#9+'Offering Can Be Gift Messaged'+#9+'Is'+
'Gift Wrap Available'+#9+'Is Discontinued by Manufacturer'+#9+'Registered Parameter'+#9+'Max Order Quantity'+#9+'Offering Release Date');
  exported.Add('feed_product_type'+#9+'item_sku'+#9+'external_product_id'+#9+'external_product_id_type'+#9+'brand_name'+#9+'item_name'+#9+'color_name'+#9+'color_map'+#9+'department_name'+#9+'size_name'+#9+'size_map'+#9+'is_adult_product'+#9+'outer_material_type1'+#9+'outer_material_type2'+#9+'outer_material_type3'+#9+'outer_material_type4'+#9+'outer'+
'_material_type5'+#9+'material_composition1'+#9+'material_composition2'+#9+'material_composition3'+#9+'material_composition4'+#9+'material_composition5'+#9+'material_composition6'+#9+'material_composition7'+#9+'material_composition8'+#9+'material_composition9'+#9+'material_composition10'+#9+'standard_price'+#9+'qua'+
'ntity'+#9+'merchant_shipping_group_name'+#9+'main_image_url'+#9+'other_image_url1'+#9+'other_image_url2'+#9+'other_image_url3'+#9+'swatch_image_url'+#9+'main_offer_image'+#9+'offer_image'+#9+'parent_child'+#9+'parent_sku'+#9+'relationship_type'+#9+'variation_theme'+#9+'item_type'+#9+'product_description'+#9+'model'+#9+'update_delete'+#9+''+
'part_number'+#9+'manufacturer'+#9+'bullet_point1'+#9+'bullet_point2'+#9+'bullet_point3'+#9+'bullet_point4'+#9+'bullet_point5'+#9+'generic_keywords1'+#9+'generic_keywords2'+#9+'generic_keywords3'+#9+'generic_keywords4'+#9+'generic_keywords5'+#9+'rise_height'+#9+'rise_height_unit_of_measure'+#9+'style_name'+#9+'material_type'+#9+'websi'+
'te_shipping_weight'+#9+'website_shipping_weight_unit_of_measure'+#9+'item_weight_unit_of_measure'+#9+'item_weight'+#9+'item_length'+#9+'item_width'+#9+'item_height'+#9+'item_volume'+#9+'item_volume_unit_of_measure'+#9+'item_dimensions_unit_of_measure'+#9+'fulfillment_center_id'+#9+'package_height'+#9+'package_widt'+
'h'+#9+'package_length'+#9+'package_weight'+#9+'package_weight_unit_of_measure'+#9+'package_dimensions_unit_of_measure'+#9+'cpsia_cautionary_statement'+#9+'cpsia_cautionary_description'+#9+'fabric_type'+#9+'import_designation'+#9+'country_of_origin'+#9+'prop_65'+#9+'batteries_required'+#9+'are_batteries_included'+#9+'ba'+
'ttery_type'+#9+'number_of_batteries'+#9+'number_of_lithium_metal_cells'+#9+'number_of_lithium_ion_cells'+#9+'lithium_battery_packaging'+#9+'lithium_battery_energy_content'+#9+'lithium_battery_energy_content_unit_of_measure'+#9+'lithium_battery_weight'+#9+'lithium_battery_weight_unit_of_measure'+#9+''+
'list_price'+#9+'condition_type'+#9+'condition_note'+#9+'product_tax_code'+#9+'fulfillment_latency'+#9+'product_site_launch_date'+#9+'merchant_release_date'+#9+'restock_date'+#9+'sale_price'+#9+'sale_from_date'+#9+'sale_end_date'+#9+'offering_end_date'+#9+'max_aggregate_ship_quantity'+#9+'item_package_quantity'+#9+'number_of'+
'_items'+#9+'offering_can_be_gift_messaged'+#9+'offering_can_be_giftwrapped'+#9+'is_discontinued_by_manufacturer'+#9+'missing_keyset_reason'+#9+'max_order_quantity'+#9+'offering_start_date');

  for i := 3 to form1.StringGrid1.RowCount-1 do
    begin
      for j := 0 to form1.StringGrid1.ColCount-1 do
        begin
          if j=0 then
            line:= form1.StringGrid1.Cells[j,i]
          else
            line:= line + #9 + form1.StringGrid1.Cells[j,i];
        end;
      exported.Add(line);
    end;
  line:= 'Mercury_data\Export\'+Item.Title+'\'+Item.ID+'_tab.csv';
  exported.SaveToFile(line);
  result:= line;
end;

function GridExport_COMMA:string;
var i,j:integer;
    exported: TStringList;
    line: string;
begin
  exported:= TStringList.Create;
  exported.Add('TemplateType=fptcustom,Version=2017.1212,TemplateSignature=UEFOVFM=,The top 3 rows are for Amazon.com use only. Do not modify or delete the top 3 rows.,,,,,,,,,,,,,,,,,,,,,,,,,,,,Images,,,,,,Variation,,,,Basic,,,,,,Discovery,,,,,,,,,,Ungrouped,,,,Dimensio'+
'ns,,,,,,,,,,Fulfillment,,,,,,,Compliance,,,,,,,,,,,,,,,,,Offer,,,,,,,,,,,,,,,,,,,,');
  exported.Add('Product Type,Seller SKU,Product ID,Product ID Type,Brand Name,Product Name,Color,Color Map,Department,Size,Size Map,Is Adult Product,Outer Material Type,Outer Material Type,Outer Material Type,Outer Material Type,Outer Material Type,Material Composition,M'+
'aterial Composition,Material Composition,Material Composition,Material Composition,Material Composition,Material Composition,Material Composition,Material Composition,Material Composition,Standard Price,Quantity,Shipping-Template,Main Image URL,Other Imag'+
'e URL,Other Image URL,Other Image URL,Swatch Image URL,Main Offer Image,Offer Image,Parentage,Parent SKU,Relationship Type,Variation Theme,Item Type Keyword,Product Description,Style Number,Update Delete,Manufacturer Part Number,Manufacturer,Key Product F'+
'eatures,Key Product Features,Key Product Features,Key Product Features,Key Product Features,Search Terms,Search Terms,Search Terms,Search Terms,Search Terms,Rise Height,Rise Height Unit Of Measure,Style,material_type,Shipping Weight,Website Shipping Weigh'+
't Unit Of Measure,Item Weight Unit Of Measure,Item Weight,Item Length,Item Width,Item Height,item_volume,Item Volume Unit Of Measure,Item Dimensions Unit Of Measure,Fulfillment Center ID,Package Height,Package Width,Package Length,Package Weight,Package W'+
'eight Unit Of Measure,Package Dimensions Unit Of Measure,Cpsia Warning,CPSIA Warning Description,Fabric Type,Import Designation,Country of Publication,Consumer Notice,batteries_required,are_batteries_included,Battery Type,Number of Batteries Required,numb'+
'er_of_lithium_metal_cells,number_of_lithium_ion_cells,lithium_battery_packaging,lithium_battery_energy_content,lithium_battery_energy_content_unit_of_measure,lithium_battery_weight,lithium_battery_weight_unit_of_measure,Manufacturer''s Suggested Retail Pri'+
'ce,Item Condition,Condition Note,Product Tax Code,Handling Time,Launch Date,Release Date,Restock Date,Sale Price,Sale Start Date,Sale End Date,Stop Selling Date,Max Aggregate Ship Quantity,Package Quantity,Number of Items,Offering Can Be Gift Messaged,Is'+
'Gift Wrap Available,Is Discontinued by Manufacturer,Registered Parameter,Max Order Quantity,Offering Release Date');
  exported.Add('feed_product_type,item_sku,external_product_id,external_product_id_type,brand_name,item_name,color_name,color_map,department_name,size_name,size_map,is_adult_product,outer_material_type1,outer_material_type2,outer_material_type3,outer_material_type4,outer'+
'_material_type5,material_composition1,material_composition2,material_composition3,material_composition4,material_composition5,material_composition6,material_composition7,material_composition8,material_composition9,material_composition10,standard_price,qua'+
'ntity,merchant_shipping_group_name,main_image_url,other_image_url1,other_image_url2,other_image_url3,swatch_image_url,main_offer_image,offer_image,parent_child,parent_sku,relationship_type,variation_theme,item_type,product_description,model,update_delete,'+
'part_number,manufacturer,bullet_point1,bullet_point2,bullet_point3,bullet_point4,bullet_point5,generic_keywords1,generic_keywords2,generic_keywords3,generic_keywords4,generic_keywords5,rise_height,rise_height_unit_of_measure,style_name,material_type,websi'+
'te_shipping_weight,website_shipping_weight_unit_of_measure,item_weight_unit_of_measure,item_weight,item_length,item_width,item_height,item_volume,item_volume_unit_of_measure,item_dimensions_unit_of_measure,fulfillment_center_id,package_height,package_widt'+
'h,package_length,package_weight,package_weight_unit_of_measure,package_dimensions_unit_of_measure,cpsia_cautionary_statement,cpsia_cautionary_description,fabric_type,import_designation,country_of_origin,prop_65,batteries_required,are_batteries_included,ba'+
'ttery_type,number_of_batteries,number_of_lithium_metal_cells,number_of_lithium_ion_cells,lithium_battery_packaging,lithium_battery_energy_content,lithium_battery_energy_content_unit_of_measure,lithium_battery_weight,lithium_battery_weight_unit_of_measure,'+
'list_price,condition_type,condition_note,product_tax_code,fulfillment_latency,product_site_launch_date,merchant_release_date,restock_date,sale_price,sale_from_date,sale_end_date,offering_end_date,max_aggregate_ship_quantity,item_package_quantity,number_of'+
'_items,offering_can_be_gift_messaged,offering_can_be_giftwrapped,is_discontinued_by_manufacturer,missing_keyset_reason,max_order_quantity,offering_start_date');

  for i := 3 to form1.StringGrid1.RowCount-1 do
    begin
      for j := 0 to form1.StringGrid1.ColCount-1 do
        begin
          if j=0 then
            line:= form1.StringGrid1.Cells[j,i]
          else
            line:= line + ',' + form1.StringGrid1.Cells[j,i];
        end;
      exported.Add(line);
    end;
  line:= 'Mercury_data\Export\'+Item.Title+'\'+Item.ID+'_comma.csv';
  exported.SaveToFile(line);
  result:= line;
end;

procedure ProcessBullets;
var work: TStringList;
    tmp: string;
    i: Integer;
begin
  work:= TStringList.Create;                                                    // process bullet points
  work.Delimiter:='*';
  work.StrictDelimiter:= true;
  work.DelimitedText:= stringreplace(Item.BulletPointsString,'<li>','*',[rfReplaceAll, rfIgnoreCase]);
  tmp:= '';
  for i := work.Count-1 downto 0 do
    if RemoveTags(work[i])='' then
      work.Delete(i);

  for i := 0 to work.Count-1 do
    begin
      if i=work.Count-1 then break;
      if ContainsText(RemoveTags(work[i]),'wash') then continue;

      case i of
      0..3: item.Bullets[i+1]:= RemoveTags(work[i]);
      4:    tmp:= RemoveTags(work[i]);
      5..50:tmp:= tmp + '. '+RemoveTags(work[i]);
      end;
    end;

  if not(tmp='') then
    begin
      if tmp[1]='.' then
        delete(tmp,1,2);
      item.Bullets[5]:= tmp;
    end;

  work.Free;
end;

procedure RecreateMissingVariants;
var i,j,k,pointer: integer;
    sizes,colours: TStringList;
    found: boolean;
begin
  sizes:=   TStringList.Create;
  colours:= TStringList.Create;
  sizes.Sorted:=        true;
  colours.Sorted:=      true;
  sizes.Duplicates:=    dupIgnore;
  colours.Duplicates:=  dupIgnore;

  for i := 0 to length(vars)-1 do
    begin
      if trim(vars[i].Size)<>'' then
        sizes.Add(vars[i].Size);
      if trim(vars[i].ColorCode)<>'' then
        colours.Add(vars[i].ColorCode);
    end;

  if (sizes.Count*colours.Count)>length(vars) then
    begin
      Log('Missing variants found, recreating '+inttostr((sizes.Count*colours.Count)-length(vars))+' items using custom UPC list');
      SetLength(vars,sizes.Count*colours.Count);
      pointer:=0;
      for i := 0 to length(vars)-1 do
        if not Assigned(vars[i]) then
          begin
            if pointer=0 then
              pointer:= i;
            vars[i]:= fVariant.Create;
          end;

      for i := 0 to sizes.Count-1 do
        for j := 0 to colours.Count-1 do
          begin
            found:= false;
            for k := 0 to length(vars)-1 do
              if (vars[k].Size=sizes[i]) and (vars[k].ColorCode=colours[j]) then
                found:= true;
            if not found then
              begin
                vars[pointer].Size:= sizes[i];
                vars[pointer].ColorCode:= colours[j];
                vars[pointer].UPC:= WUPCtoUPC(nextUPC);
                IncreaseUPC;
                inc(pointer);
              end;
          end;
    end;

  sizes.Free;
  colours.Free;
end;

procedure GrabImages;
var i:integer;
    tmp: string;
begin
  CreateFolder('Mercury_data\Export\'+Item.Title,true);
  Log('Downloading '+inttostr(images.Count)+' images and preparing the output file.');     // get images
  for i := 0 to images.Count-1 do
    begin
      Progress(25+round((50/images.Count)*i));
      tmp:= 'Mercury_data\Export\'+Item.Title+'\'+ExtractData(Images[i],'asr/','*end*');
      if not FileExists(tmp) then
        begin
          if DownloadImage(Images[i],tmp) then
            Log('Image downloaded: '+inttostr(i+1)+'/'+inttostr(images.count))
          else
            Log('ERROR downloading image #'+inttostr(i+1));
        end
      else
        Log('Image already downloaded, skipping');
    end;

//  Item.MainImage:= FindLargestFile(GetCurrentDir+'\Mercury_data\Export\'+Item.Title,'*.jpeg');
end;

procedure TForm1.Button1Click(Sender: TObject);
var work,variants: TStringList;
    i, pointer: Integer;
    Variant: fVariant;
    tmp: string;
    main_idml,main_primary:string;
    Save_Cursor : TCursor;
    JSONObj: ISuperObject;
    item1, item2 : IMember;
begin
  if not(ContainsText(form1.Edit1.Text,'https://www.walmart.com')) then
    begin
      form1.Edit1.Text:= 'No valid Walmart links found!';
      exit;
    end;

  SetLength(vars,0);
  form1.StringGrid1.Visible:= false;
  form1.CEFWindowParent1.Visible:=true;
  button1.Enabled:= false;
  button2.Enabled:= false;
  button3.Enabled:= false;
  Save_Cursor:= Screen.Cursor;
  Screen.Cursor := crHourGlass;
  Progress(5);
  ReadINI;
  form1.logs.Clear;
  Log('Walmart link found, starting the process');
  if form1.CheckBox1.Checked then
    Chromium1.LoadURL(form1.Edit1.Text);

  if not local then
    begin
      if ContainsText(form1.Edit1.Text,'?') then
        begin
          Log('Removing affiliate URL parameters from the link');
          form1.Edit1.Text:= ExtractData(form1.Edit1.Text,'*start*','?');
        end;
      Progress(10);

      if GetWebsite(form1.Edit1.Text,'Mercury_data\Temp\') then
        begin
          work:= TStringList.Create;
          work.LoadFromFile('Mercury_data\Temp\grabbed.html');
        end
      else
        begin
          Log('ERROR: Couldn''t download the page');
          RestoreDesktop(Save_Cursor);
          exit;
        end;

      Progress(20);
      work:= TStringList.Create;
      work.LoadFromFile('Mercury_data\Temp\grabbed.html');

      JSONObj:= nil;
      for i := 0 to work.Count-1 do
        if ContainsText(work[i],'INITIAL_STATE__ = {"uuid"') then
          begin
            tmp:= ExtractData(work[i],'INITIAL_STATE__ = ',';};');
            JSONObj := SO(tmp);
            JSONObj.SaveTo('Mercury_data\Temp\temp.json');
            break;
          end;

      if JSONObj=nil then
        begin
          Log('ERROR: Page downloaded, but no item data found.');
          RestoreDesktop(Save_Cursor);
          exit;
        end;
    end
  else
    begin
      work:= TStringList.Create;                                                    // speed loader, local
      work.LoadFromFile('Mercury_data\Temp\temp.json');
      JSONObj:= SO(work[0]);
    end;

  Item:= fItem.Create;
  Variant:= fVariant.Create;
  variants:= TStringList.Create;
  images:= TStringList.Create;
  images.Sorted:=true;
  images.Duplicates:= dupIgnore;

  try
    main_primary:= JSONObj['product.primaryProduct'].AsString;               // basics
    for item1 in JSONObj['product.idmlMap'].AsObject do
      begin
        main_idml:= item1.Name;
        break;
      end;

    Item.ID:=           JSONObj['productId'].AsString;
    Item.Title:=        JSONObj['product.products."'+main_primary+'".productAttributes.productName'].AsString;
    Item.Brand:=        JSONObj['product.products."'+main_primary+'".productAttributes.brand'].AsString;
    Item.Description:=  JSONObj['product.idmlMap."' +main_idml+'".modules.ShortDescription.product_short_description.DisplayValue'].AsString;
    Item.BulletPointsString:= JSONObj['product.products."'+main_primary+'".productAttributes.detailedDescription'].AsString;

    if item.BulletPointsString='' then
      Item.BulletPointsString:= JSONObj['product.products."'+main_idml+'".productAttributes.detailedDescription'].AsString;
    Item.BulletPointsString:= stringreplace(Item.BulletPointsString,'&nbsp;','',[rfReplaceAll, rfIgnoreCase]);
    ProcessBullets;

    if item.Title='' then
      item.Title:= JSONObj['product.products."'+main_idml+'".productAttributes.productName'].AsString;
    item.Title:= stringreplace(Item.Title,'/','-',[rfReplaceAll, rfIgnoreCase]);

    if item.Description='' then
      Log('No item description specified');

    for item1 in JSONObj['product.idmlMap."' +main_idml+'".modules.Specifications.specifications.values[0]'].AsArray do
      begin
        if FieldExists(item1,'gender') then
          item.Gender:= item1.AsObject['gender.displayValue'].AsString;
        if FieldExists(item1,'fabric_material') then
          item.FabricString:= item1.AsObject['fabric_material.displayValue'].AsString;
        if FieldExists(item1,'dimension') then
          Item.DimensionsString:= item1.AsObject['dimension.displayValue'].AsString;
        if FieldExists(item1,'brand') and (item.brand='') then
          Item.Brand:= item1.AsObject['brand.displayValue'].AsString;
      end;
    Item.Dept:=  MatchDepartment(JSONObj['product.idmlMap."' +main_idml+'".modules.GeneralInfo.category_path_name.displayValue'].AsString,Item.Gender);
    DecompileFabrics;
    DecompileDimensions;
    item.Weight:= '12';

    for item1 in JSONObj['product.offers'].AsObject do
      begin
        Item.Price:= JSONObj['product.offers."'+item1.Name+'".PricesInfo.priceMap.WAS.price'].AsFloat;
        if Item.Price>0 then
          break;
      end;

    if Item.Price=0 then
    begin
      Log('Couldn''t locate "WAS" price in JSON, trying again looking for "CURRENT" price');
      foR item1 in JSONObj['product.offers'].AsObject do
        begin
          Item.Price:= JSONObj['product.offers."'+item1.Name+'".PricesInfo.priceMap.CURRENT.price'].AsFloat;
          if Item.Price>0 then
            break;
        end;
    end;

    pointer:= 0;
    for item1 in JSONObj['product.products'].AsObject do                        // variants
    begin
      if FieldExists(item1,'upc') then
        Variant.UPC:= RemoveQuotes(JSONObj['product.products."'+item1.Name+'".upc'].ToString)
      else
        if FieldExists(item1,'wupc') then
          begin
            Variant.UPC:= RemoveQuotes(JSONObj['product.products."'+item1.Name+'".wupc'].ToString);
            Log('UPC not found! Trying to calculate it manually using WUPC instead.');
            WUPCtoUPC(Variant.UPC);
          end
        else
          begin
            Log('UPC nor WUPC found, skipping item.');
            continue;
          end;

      while (length(variant.UPC)<13) do
        variant.UPC:= '0' + variant.UPC;

      if not FieldExists(item1,'variants') then
        begin
          Log('UPC found, but no item data, skipping item variant.');
          variants.add(',,,"'+Variant.UPC+'",');
          continue;
        end;

      for item2 in JSONObj['product.products."'+item1.Name+'".variants'].AsObject do
        begin
          if item2.name='size' then
            begin
              tmp:= RemoveQuotes(JSONObj['product.products."'+item1.Name+'".variants.size'].ToString);
              Variant.Size:= JSONObj['product.variantCategoriesMap."'+main_primary+'".size.variants.'+tmp+'.name'].ToString;
            end;
          if item2.name='clothing_size' then
            begin
              tmp:= RemoveQuotes(JSONObj['product.products."'+item1.Name+'".variants.clothing_size'].ToString);
              Variant.Size:= JSONObj['product.variantCategoriesMap."'+main_primary+'".clothing_size.variants.'+tmp+'.name'].ToString;
            end;
          Variant.Size:= RemoveQuotes(Variant.Size);
        end;

      if Variant.Size='' then
        begin
          Log('Couldn''t read sizing info, skipping item variant.');
          variants.add(',,,"'+Variant.UPC+'",');
          continue;
        end;

      pointer:= Length(vars)+1;
      SetLength(vars,pointer);
      vars[pointer-1]:= fVariant.Create;
      vars[pointer-1].UPC:= variant.UPC;
      vars[pointer-1].ColorCode:= RemoveQuotes(JSONObj['product.products."'+item1.Name+'".variants.actual_color'].ToString);
      vars[pointer-1].Size:= Variant.Size;
      vars[pointer-1].Size_map:= 'SOON';
      variants.add(Variant.UPC);
    end;

    RecreateMissingVariants;

    for i := 0 to length(vars)-1 do                                             // assign images
      begin
        if vars[i].ColorCode='' then continue;
        for item2 in JSONObj['product.variantCategoriesMap."'+main_primary+'".actual_color.variants."'+vars[i].ColorCode+'".images'].AsArray do
          if vars[i].Image1='' then
            vars[i].Image1:= GetImage(JSONObj,RemoveQuotes(item2.ToString))
          else
            begin
              vars[i].Image2:= GetImage(JSONObj,RemoveQuotes(item2.ToString));
              break;
            end;
      end;

    for i := 0 to length(vars)-1 do                                             // assign color pretty names
      begin
        if vars[i].ColorCode='' then continue;
        vars[i].Color:= RemoveQuotes(JSONObj['product.variantCategoriesMap."'+main_primary+'".actual_color.variants."'+vars[i].ColorCode+'".name'].ToString);
        vars[i].Color_primary:= ColorToPrimary(vars[i].Color);
      end;

  except
    on E: Exception do
      begin
        Variant.Free;                                                           // if there are issues with JSON parsing, halt
        Item.Free;
        variants.Free;
        if local then
          work.Free;
        Progress(0);
        RestoreDesktop(Save_Cursor);
        Log('There was a problem parsing the JSON data! Cannot proceed with data extraction, stopping.');
        AppendLogFile('problem-links.log',form1.Edit1.Text);
        Log('Problematic link was added to the list saved in problem-links.log');
        messagedlg('There was a problem parsing the JSON data!',mtError, [mbCancel], 0);
        exit;
      end;
  end;

  Variant.Free;
  lastFile:= Item.Title;
  Progress(25);

  GrabImages;

  SetLength(UPCs, variants.Count);                                              // sort by UPC values
  for i := 0 to variants.Count-1 do
    UPCs[i]:= stringreplace(ExtractData(variants[i],',*','*,'),'*','',[rfReplaceAll, rfIgnoreCase]);
  Sort;
  Progress(85);
  sleep(100);

  item.Category:= AssignCategory(item.Title);
  item.Feed:= AssignFeed(item.Category);
  form2.Image1.Picture:= nil;
  form2.Image2.Picture:= nil;
  form2.Image3.Picture:= nil;
  form2.Image4.Picture:= nil;
  form2.Image5.Picture:= nil;

  form2.ShowModal;
  if lastFile<>Item.Title then
    TDirectory.Move(GetCurrentDir+'\Mercury_data\Export\'+lastFile,GetCurrentDir+'\Mercury_data\Export\'+Item.Title);

  Log(inttostr(length(vars))+' total variants found');
  form1.StringGrid1.RowCount:= length(vars)+4;

  GridHeaders;
  GridOutput;
  Progress(100);
  sleep(500);
  application.ProcessMessages;

  try
    lastFile:= GridExport_TAB;
    Log('Output file created: '+lastFile);
    lastFile:= GridExport_COMMA;
    Log('Output file created: '+lastFile);
  except
    on E:Exception do
      ShowMessage('Couldn''t create the output file. Make sure it is not open externally in Excel and try again.');
  end;

  IncreaseParentID;
  beep;
  variants.Free;
  Item.Free;

  RestoreDesktop(Save_Cursor);
  form1.Button3.Enabled:= true;
  Log('ALL DONE! Waiting for new instructions');
  sleep(1000);
  Progress(0);
end;

procedure TForm1.Button2Click(Sender: TObject);
var tmp:string;
begin
  button2.Enabled:= false;
  tmp:= Clipboard.AsText;
  if ContainsText(tmp,'https://www.walmart.com') then
    begin
      form1.Edit1.Text:= tmp;
      form1.logs.Clear;
      Log('Walmart link found in clipboard, starting auto-download');
      form1.Button1.Click;
    end
  else
    begin
      form1.Edit1.Text:= 'No valid Walmart links found!';
      button2.Enabled:= true;
    end;
end;

procedure TForm1.Button3Click(Sender: TObject);
var Save_Cursor : TCursor;
begin
  Save_Cursor:= Screen.Cursor;
  Screen.Cursor:= crHourGlass;
  form1.StringGrid1.Visible:= false;
  form1.CEFWindowParent1.Visible:= true;
  Log('');
  //tmp:= 'Mercury_data\Export\Danskin Now Women''s Active Bungee Tank with Mesh Detail\55089733.csv';
  if not(ShellExecute(Handle,'open', PWideChar(lastFile), nil,nil, SW_SHOWNORMAL)>32) then
    Log('Couldn''t open Excel.')
  else
    Log('Excel starting, please wait');
  RestoreDesktop(Save_Cursor);
end;

procedure TForm1.Chromium1TitleChange(Sender: TObject;
  const browser: ICefBrowser; const title: ustring);
begin
  if title<>'about:blank' then
    begin
      Edit1.Text:= chromium1.DocumentURL;
      caption := 'Mercury - ' + title;
    end;
end;

procedure TForm1.Edit1Click(Sender: TObject);
begin
  form1.Edit1.Text:='';
end;

procedure TForm1.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  chromium1.StopLoad;

  primary_colours.Free;
  images.Free;

  big_colours.Free;
  big_sizes.Free;
  big_categories.Free;
  big_feeds.free;

  CanClose:= true;
end;

procedure TForm1.IdHTTP1Status(ASender: TObject; const AStatus: TIdStatus;
  const AStatusText: string);
begin
  Log(AStatusText);
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  if not(Chromium1.CreateBrowser(CEFWindowParent1, '')) then
    Timer1.Enabled := True
  else
    begin
      button1.Enabled:= true;
      button2.Enabled:= true;
      Log('Chromium ready.');
    end;
end;






function DictionariesInit:boolean;
begin
  result:= true;

  primary_colours:= TStringList.Create;
  primary_colours.Add('Beige');
  primary_colours.Add('Black');
  primary_colours.Add('Blue');
  primary_colours.Add('Bronze');
  primary_colours.Add('Brown');
  primary_colours.Add('Gold');
  primary_colours.Add('Green');
  primary_colours.Add('Grey');
  primary_colours.Add('Metallic');
  primary_colours.Add('Multicoloured');
  primary_colours.Add('Off-White');
  primary_colours.Add('Orange');
  primary_colours.Add('Pink');
  primary_colours.Add('Purple');
  primary_colours.Add('Red');
  primary_colours.Add('Silver');
  primary_colours.Add('Transparent');
  primary_colours.Add('Turquoise');
  primary_colours.Add('White');
  primary_colours.Add('Yellow');
  primary_colours.Add('Gray');

  big_colours:= TStringList.Create;
  if fileExists('Mercury_data\colours.dict') then
    big_colours.LoadFromFile('Mercury_data\colours.dict')
  else
    begin
      Log('Colours dictionary file (colours.dict) missing! Reverting to default.');
      big_colours.Add('Blue,Azure');
      big_colours.SaveToFile('Mercury_data\colours.dict');
      result:= false;
    end;

  big_sizes:= TStringList.Create;
  if fileExists('Mercury_data\sizes.dict') then
    big_sizes.LoadFromFile('Mercury_data\sizes.dict')
  else
    begin
      Log('Sizes dictionary file (sizes.dict missing! Reverting to default.');
      // Pretty Size,	Amazon Size,	Size Map
      big_sizes.Add('XXS,XX-Small,XX-Small');
      big_sizes.Add('XS,X-Small,X-Small');
      big_sizes.Add('S,Small,Small');
      big_sizes.Add('M,Medium,Medium');
      big_sizes.Add('L,Large,Large');
      big_sizes.Add('XL,X-Large,X-Large');
      big_sizes.Add('XXL,XX-Large,XX-Large');
      big_sizes.Add('3XL,XXX-Large,XXX-Large');
      big_sizes.Add('4XL,XXXX-Large,XXXX-Large');
      big_sizes.Add('5XL,XXXXX-Large,XXXXX-Large');
      big_sizes.Add('6XL,XXXXXX-Large,XXXXXX-Large');
      big_sizes.Add('1X,Plus 1X,X-Large');
      big_sizes.Add('2X,Plus 2X,XX-Large');
      big_sizes.Add('3X,Plus 3X,XXX-Large');
      big_sizes.Add('4X,Plus 4X,XXXX-Large');
      big_sizes.Add('5X,Plus 5X,XXXXX-Large');
      big_sizes.Add('0,0,X-Small');
      big_sizes.Add('2,2,Small');
      big_sizes.Add('4,4,Small');
      big_sizes.Add('6,6,Medium');
      big_sizes.Add('8,8,Medium');
      big_sizes.Add('10,10,Large');
      big_sizes.Add('12,12,Large');
      big_sizes.Add('14,14,X-Large');
      big_sizes.Add('16,16,X-Large');
      big_sizes.Add('18,18,XX-Large');
      big_sizes.Add('20,20,XXX-Large');
      big_sizes.Add('22,22,XXX-Large');
      big_sizes.Add('24,24,XXXX-Large');
      big_sizes.Add('26,26,XXXX-Large');
      big_sizes.Add('28,28,XXXXX-Large');
      big_sizes.Add('16W,16W,X-Large');
      big_sizes.Add('18W,18W,XX-Large');
      big_sizes.Add('20W,20W,XXX-Large');
      big_sizes.Add('22W,22W,XXX-Large');
      big_sizes.Add('24W,24W,XXXX-Large');
      big_sizes.Add('26W,26W,XXXX-Large');
      big_sizes.Add('28W,28W,XXXXX-Large');
      big_sizes.Add('16W Petite,16W Petite,X-Large');
      big_sizes.Add('18W Petite,18W Petite,XX-Large');
      big_sizes.Add('20W Petite,20W Petite,XXX-Large');
      big_sizes.Add('22W Petite,22W Petite,XXX-Large');
      big_sizes.Add('24W Petite,24W Petite,XXXX-Large');
      big_sizes.Add('26W Petite,26W Petite,XXXX-Large');
      big_sizes.Add('28W Petite,28W Petite,XXXXX-Large');
      big_sizes.Add('XXS Petite,XX-Small,XX-Small');
      big_sizes.Add('XS Petite,X-Small,X-Small');
      big_sizes.Add('S Petite,Small,Small');
      big_sizes.Add('M Petite,Medium,Medium');
      big_sizes.Add('L Petite,Large,Large');
      big_sizes.Add('XL Petite,X-Large,X-Large');
      big_sizes.Add('XXL Petite,XX-Large,XX-Large');
      big_sizes.Add('2XL Petite,XX-Large,XX-Large');
      big_sizes.Add('2XL,XX-Large,XX-Large');
      big_sizes.Add('1XP,Petite Plus 1X,X-Large');
      big_sizes.Add('2XP,Petite Plus 2X,XX-Large');
      big_sizes.Add('3XP,Petite Plus 3X,XXX-Large');
      big_sizes.Add('4XP,Petite Plus 4X,XXXX-Large');
      big_sizes.Add('5XP,Petite Plus 5X,XXXXX-Large');
      big_sizes.Add('28X28,28X28,X-Small');
      big_sizes.Add('28X30,28X30,X-Small');
      big_sizes.Add('28X32,28X32,X-Small');
      big_sizes.Add('28X34,28X34,X-Small');
      big_sizes.Add('29X28,29X28,X-Small');
      big_sizes.Add('29X30,29X30,X-Small');
      big_sizes.Add('29X32,29X32,X-Small');
      big_sizes.Add('29X34,29X34,X-Small');
      big_sizes.Add('30X28,30X28,Small');
      big_sizes.Add('30X30,30X30,Small');
      big_sizes.Add('30X32,30X32,Small');
      big_sizes.Add('30X34,30X34,Small');
      big_sizes.Add('32X28,32X28,Small');
      big_sizes.Add('32X30,32X30,Small');
      big_sizes.Add('32X32,32X32,Small');
      big_sizes.Add('32X34,32X34,Small');
      big_sizes.Add('32X36,32X36,Small');
      big_sizes.Add('34X28,34X28,Medium');
      big_sizes.Add('34X30,34X30,Medium');
      big_sizes.Add('34X32,34X32,Medium');
      big_sizes.Add('34X34,34X34,Medium');
      big_sizes.Add('34X36,34X36,Medium');
      big_sizes.Add('36X28,36X28,Large');
      big_sizes.Add('36X30,36X30,Large');
      big_sizes.Add('36X32,36X32,Large');
      big_sizes.Add('36X34,36X34,Large');
      big_sizes.Add('36X36,36X36,Large');
      big_sizes.Add('38X28,38X28,X-Large');
      big_sizes.Add('38X30,38X30,X-Large');
      big_sizes.Add('38X32,38X32,X-Large');
      big_sizes.Add('38X34,38X34,X-Large');
      big_sizes.Add('38X36,38X36,X-Large');
      big_sizes.Add('40X28,40X28,X-Large');
      big_sizes.Add('40X30,40X30,X-Large');
      big_sizes.Add('40X32,40X32,X-Large');
      big_sizes.Add('40X32,40X32,X-Large');
      big_sizes.Add('42X28,42X28,XX-Large');
      big_sizes.Add('42X30,42X30,XX-Large');
      big_sizes.Add('42X32,42X32,XX-Large');
      big_sizes.Add('44X28,44X28,XX-Large');
      big_sizes.Add('44X30,44X30,XX-Large');
      big_sizes.Add('44X32,44X32,XX-Large');
      big_sizes.Add('46X28,46X28,XXX-Large');
      big_sizes.Add('46X30,46X30,XXX-Large');
      big_sizes.Add('46X32,46X32,XXX-Large');
      big_sizes.Add('48X28,48X28,XXX-Large');
      big_sizes.Add('48X30,48X30,XXX-Large');
      big_sizes.Add('48X32,48X32,XXX-Large');
      big_sizes.Add('50X28,50X28,XXXX-Large');
      big_sizes.Add('50X30,50X30,XXXX-Large');
      big_sizes.Add('50X32,50X32,XXXX-Large');
      big_sizes.Add('52X28,52X28,XXXX-Large');
      big_sizes.Add('52X30,52X30,XXXX-Large');
      big_sizes.Add('52X32,52X32,XXXX-Large');
      big_sizes.Add('54X28,54X28,XXXXX-Large');
      big_sizes.Add('54X30,54X30,XXXXX-Large');
      big_sizes.Add('54X32,54X32,XXXXX-Large');
      big_sizes.Add('56X28,56X28,XXXXXX-Large');
      big_sizes.Add('56X30,56X30,XXXXXX-Large');
      big_sizes.Add('56X32,56X32,XXXXXX-Large');
      big_sizes.SaveToFile('Mercury_data\sizes.dict');
      result:= false;
    end;

  big_feeds:= TStringList.Create;
  if fileExists('Mercury_data\feeds.dict') then
    big_feeds.LoadFromFile('Mercury_data\feeds.dict')
  else
    begin
      Log('Feeds dictionary file (feeds.dict missing! Reverting to default.');

        big_feeds.Add('athletic-hoodies,outerwear');
        big_feeds.Add('athletic-leggings,pants');
        big_feeds.Add('athletic-maternity-hoodies,outerwear');
        big_feeds.Add('athletic-pants,pants');
        big_feeds.Add('athletic-shirts,shirt');
        big_feeds.Add('athletic-shorts,shorts');
        big_feeds.Add('athletic-skirts,dress');
        big_feeds.Add('athletic-skorts,shorts');
        big_feeds.Add('athletic-socks,sockshosiery');
        big_feeds.Add('athletic-sweatpants,pants');
        big_feeds.Add('athletic-sweatshirts,outerwear');
        big_feeds.Add('athletic-sweatsuits,outerwear');
        big_feeds.Add('athletic-warm-up-and-track-jackets,outerwear');
        big_feeds.Add('baseball-caps,hat');
        big_feeds.Add('bathrobes,sleepwear');
        big_feeds.Add('bikini-underwear,underwear');
        big_feeds.Add('blazers-and-sports-jackets,outerwear');
        big_feeds.Add('blouses,shirt');
        big_feeds.Add('body-shapers-undergarments,underwear');
        big_feeds.Add('boxer-briefs,underwear');
        big_feeds.Add('boxer-shorts,underwear');
        big_feeds.Add('boy-shorts-panties,underwear');
        big_feeds.Add('bra-extenders,bra');
        big_feeds.Add('bra-inserts,bra');
        big_feeds.Add('bras,bra');
        big_feeds.Add('briefs-underwear,underwear');
        big_feeds.Add('button-down-shirts,shirt');
        big_feeds.Add('cardigan-sweaters,sweater');
        big_feeds.Add('cargo-shorts,shorts');
        big_feeds.Add('casual-pants,pants');
        big_feeds.Add('chemises,underwear');
        big_feeds.Add('childrens-costumes,costume');
        big_feeds.Add('cold-weather-gloves,outerwear');
        big_feeds.Add('cold-weather-hats,outerwear');
        big_feeds.Add('cold-weather-mittens,outerwear');
        big_feeds.Add('cold-weather-scarves,outerwear');
        big_feeds.Add('denim-shorts,shorts');
        big_feeds.Add('dresses,dress');
        big_feeds.Add('dress-pants,pants');
        big_feeds.Add('dress-socks,sockshosiery');
        big_feeds.Add('fashion-board-shorts,swimwear');
        big_feeds.Add('fashion-one-piece-swimsuits,swimwear');
        big_feeds.Add('fashion-skorts,shorts');
        big_feeds.Add('fashion-swimsuit-bottoms-separates,swimwear');
        big_feeds.Add('fashion-swim-trunks,swimwear');
        big_feeds.Add('fashion-tankini-sets,swimwear');
        big_feeds.Add('fashion-tankini-tops,swimwear');
        big_feeds.Add('fashion-t-shirts,shirt');
        big_feeds.Add('fashion-two-piece-swimsuits,swimwear');
        big_feeds.Add('fashion-vests,outerwear');
        big_feeds.Add('faux-leather-outerwear-jackets,outerwear');
        big_feeds.Add('flat-front-shorts,shorts');
        big_feeds.Add('fleece-outerwear-jackets,outerwear');
        big_feeds.Add('fleece-outerwear-vests,outerwear');
        big_feeds.Add('henley-shirts,shirt');
        big_feeds.Add('hipster-panties,underwear');
        big_feeds.Add('jeans,pants');
        big_feeds.Add('jumpsuits-apparel,pants');
        big_feeds.Add('leather-outerwear-jackets,outerwear');
        big_feeds.Add('leggings-pants,pants');
        big_feeds.Add('leg-warmers,pants');
        big_feeds.Add('medical-scrubs-pants,pants');
        big_feeds.Add('medical-scrubs-shirts,shirt');
        big_feeds.Add('nightgowns,sleepwear');
        big_feeds.Add('nursing-bras,bra');
        big_feeds.Add('outerwear-jackets,outerwear');
        big_feeds.Add('overalls,pants');
        big_feeds.Add('pajama-bottoms,sleepwear');
        big_feeds.Add('pajama-sets,sleepwear');
        big_feeds.Add('pajama-tops,sleepwear');
        big_feeds.Add('pants,pants');
        big_feeds.Add('pantyhose,sockshosiery');
        big_feeds.Add('parkas,outerwear');
        big_feeds.Add('pea-coats,outerwear');
        big_feeds.Add('polo-shirts,shirt');
        big_feeds.Add('pullover-sweaters,sweater');
        big_feeds.Add('rain-jackets,outerwear');
        big_feeds.Add('school-uniform-button-down-shirts,shirt');
        big_feeds.Add('school-uniform-cardigan-sweaters,sweater');
        big_feeds.Add('school-uniform-dresses,dress');
        big_feeds.Add('school-uniform-pants,pants');
        big_feeds.Add('school-uniform-polo-shirts,shirt');
        big_feeds.Add('school-uniform-pullover-sweaters,sweater');
        big_feeds.Add('school-uniform-shorts,shorts');
        big_feeds.Add('shapewear-bodysuits,underwear');
        big_feeds.Add('shapewear-briefs,underwear');
        big_feeds.Add('shapewear-full-slips,underwear');
        big_feeds.Add('shapewear-half-slips,underwear');
        big_feeds.Add('shapewear-tops,underwear');
        big_feeds.Add('shorts,shorts');
        big_feeds.Add('shrug-sweaters,sweater');
        big_feeds.Add('skirts,dress');
        big_feeds.Add('sports-bras,bra');
        big_feeds.Add('sweater-vests,sweater');
        big_feeds.Add('tank-top-and-cami-shirts,shirt');
        big_feeds.Add('thong-underwear,underwear');
        big_feeds.Add('tights,pants');
        big_feeds.Add('trenchcoats,outerwear');
        big_feeds.Add('tunic-shirts,shirt');
        big_feeds.Add('turtleneck-sweaters,sweater');
        big_feeds.Add('undershirts,underwear');
        big_feeds.Add('underwear,underwear');
        big_feeds.Add('windbreaker-jackets,outerwear');

      big_feeds.SaveToFile('Mercury_data\feeds.dict');
      result:= false;
    end;

  big_categories:= TStringList.Create;
  if fileExists('Mercury_data\categories.dict') then
    big_categories.LoadFromFile('Mercury_data\categories.dict')
  else
    begin
      Log('Categories dictionary file (categories.dict) missing! Reverting to default.');

        big_categories.Add('athletic-hoodies,"hoodie, hooded sweatshirt"');
        big_categories.Add('athletic-leggings,"sport leggings, performance leggings, active leggings, danskin now leggings, sports tights, performance tights, active tights, yoga pants"');
        big_categories.Add('athletic-maternity-hoodies,"maternity hoodie, maternity hooded sweatshrit"');
        big_categories.Add('athletic-pants,"track pants, woven pants, performance pants, yoga pants, danskin now pants"');
        big_categories.Add('athletic-shirts,"active shirt, athletic shirt, jersey, performance tee, athletic tee, base layer"');
        big_categories.Add('athletic-shorts,"active shorts, athletic shorts, basketball shorts, performance shorts, dazzle short, mesh shorts"');
        big_categories.Add('athletic-skirts,"tennis skirt, active skirt, athletic skirt"');
        big_categories.Add('athletic-skorts,"tennis skort, activeskort, athletic skort"');
        big_categories.Add('athletic-socks,"crew socks, ankle socks, low cut socks, low-cut socks, athletic socks performance socks"');
        big_categories.Add('athletic-sweatpants,"sweatpants, jogger pants, fleece pants, joggers"');
        big_categories.Add('athletic-sweatshirts,"sweatshirt, crew sweatshirt"');
        big_categories.Add('athletic-sweatsuits,"sweatsuit, sweat suit"');
        big_categories.Add('athletic-warm-up-and-track-jackets,"warm up jacket, track jacket, athletic jacket, warm-up jacket"');
        big_categories.Add('baseball-caps,"hat, cap, mesh hat, snapback"');
        big_categories.Add('bathrobes,"robe, bath robe"');
        big_categories.Add('bikini-underwear,"bikini underwear, bikini panties"');
        big_categories.Add('blazers-and-sports-jackets,"blazer, sports jacket, sports coat"');
        big_categories.Add('blouses,"shirt, blouse, top"');
        big_categories.Add('body-shapers-undergarments,"shaper, shaping, body shaper, corset, girdle, shapewear"');
        big_categories.Add('boxer-briefs,boxer briefs');
        big_categories.Add('boxer-shorts,"boxer shorts, boxers, boxer underwear"');
        big_categories.Add('boy-shorts-panties,"boy shorts, boy cut, boy short panties"');
        big_categories.Add('bra-extenders,bra extender');
        big_categories.Add('bra-inserts,bra insert');
        big_categories.Add('bras,bra');
        big_categories.Add('briefs-underwear,"hi cut panties, briefs"');
        big_categories.Add('button-down-shirts,"dress shirt, button down shirt, oxford, button up shirt, flannel shirt, button down top"');
        big_categories.Add('cardigan-sweaters,"cardigan, open front sweater, sweater"');
        big_categories.Add('cargo-shorts,cargo shorts');
        big_categories.Add('casual-pants,"casual pants, chinos, khakis"');
        big_categories.Add('chemises,chemise');
        big_categories.Add('childrens-costumes,"boys costume, girls costume, kids costume, child costume"');
        big_categories.Add('cold-weather-gloves,gloves');
        big_categories.Add('cold-weather-hats,"beanie, tobaggan, trapper, balaclava"');
        big_categories.Add('cold-weather-mittens,mittens');
        big_categories.Add('cold-weather-scarves,scarf');
        big_categories.Add('denim-shorts,"blue jean shorts, jorts, jean shorts"');
        big_categories.Add('dresses,dress');
        big_categories.Add('dress-pants,"dress pants, slacks"');
        big_categories.Add('dress-socks,dress socks');
        big_categories.Add('fashion-board-shorts,"boardshorts, board shorts"');
        big_categories.Add('fashion-one-piece-swimsuits,"bathing suit, swimsuit"');
        big_categories.Add('fashion-skorts,skorts');
        big_categories.Add('fashion-swimsuit-bottoms-separates,bikini bottom');
        big_categories.Add('fashion-swim-trunks,"trunks, swim trunks"');
        big_categories.Add('fashion-tankini-sets,tankini');
        big_categories.Add('fashion-tankini-tops,tankini top');
        big_categories.Add('fashion-t-shirts,"tee, t-shirt"');
        big_categories.Add('fashion-two-piece-swimsuits,"bikini, bikini swimsuit, bikini bathing suit"');
        big_categories.Add('fashion-vests,vest');
        big_categories.Add('faux-leather-outerwear-jackets,"faux leather coat, faux leather jacket"');
        big_categories.Add('flat-front-shorts,"flat front shorts, khaki shorts"');
        big_categories.Add('fleece-outerwear-jackets,"fleece coat, fleece jacket"');
        big_categories.Add('fleece-outerwear-vests,"bubble vest, vest jacket"');
        big_categories.Add('henley-shirts,"henley, henley shirt"');
        big_categories.Add('hipster-panties,"hipsters, hipster panties"');
        big_categories.Add('jeans,"jeans, skinny jeans, relaxed fit jeans, staright leg jeans"');
        big_categories.Add('jumpsuits-apparel,jumpsuit');
        big_categories.Add('leather-outerwear-jackets,"leather coat, leather jacket"');
        big_categories.Add('leggings-pants,"leggings, jeggings"');
        big_categories.Add('leg-warmers,leg warmer');
        big_categories.Add('medical-scrubs-pants,"scrubs, scrub pants"');
        big_categories.Add('medical-scrubs-shirts,"scrubs, scrub shirt"');
        big_categories.Add('nightgowns,"gown, nightgown"');
        big_categories.Add('nursing-bras,nursing bra');
        big_categories.Add('outerwear-jackets,"jacket, coat"');
        big_categories.Add('overalls,"overalls, bibs"');
        big_categories.Add('pajama-bottoms,"pajama pants, pajama bottoms, sleep pants, sleep bottoms"');
        big_categories.Add('pajama-sets,"pajamas, pajama set, pjs"');
        big_categories.Add('pajama-tops,"pajama shirt, pajama top, sleep shirt, sleep top"');
        big_categories.Add('pants,"pants, straight leg, cargo pants, pant, pull-on pants"');
        big_categories.Add('pantyhose,"pantyhose, hose"');
        big_categories.Add('parkas,parka');
        big_categories.Add('pea-coats,"peacoat, pea coat"');
        big_categories.Add('polo-shirts,"polo, polo shirt"');
        big_categories.Add('pullover-sweaters,"sweater, pullover sweater, pull-over sweater"');
        big_categories.Add('rain-jackets,"rain jacket, rain coat, rainjacket, raincoat"');
        big_categories.Add('school-uniform-button-down-shirts,"school oxford shirt, school uniform button down shirt"');
        big_categories.Add('school-uniform-cardigan-sweaters,"school uniform cardigan, school uniform sweater"');
        big_categories.Add('school-uniform-dresses,"school uniform skirt, school uniform dress"');
        big_categories.Add('school-uniform-pants,"school uniform flat front pants, school uniform pants"');
        big_categories.Add('school-uniform-polo-shirts,school uniform polo shirt');
        big_categories.Add('school-uniform-pullover-sweaters,school uniform sweater');
        big_categories.Add('school-uniform-shorts,school uniform flat front shorts');
        big_categories.Add('shapewear-bodysuits,bodysuit');
        big_categories.Add('shapewear-briefs,"shaping brief, brief shapewear"');
        big_categories.Add('shapewear-full-slips,full slip');
        big_categories.Add('shapewear-half-slips,half slip');
        big_categories.Add('shapewear-tops,"cami shaper, cami shapewear"');
        big_categories.Add('shorts,shorts');
        big_categories.Add('shrug-sweaters,shrug');
        big_categories.Add('skirts,skirt');
        big_categories.Add('sports-bras,sports bra');
        big_categories.Add('sweater-vests,sweater vest');
        big_categories.Add('tank-top-and-cami-shirts,"tank, tank top, cami"');
        big_categories.Add('thong-underwear,"thongs, thong panties"');
        big_categories.Add('tights,"tights, dance tights"');
        big_categories.Add('trenchcoats,trench coat');
        big_categories.Add('tunic-shirts,tunic');
        big_categories.Add('turtleneck-sweaters,turtleneck');
        big_categories.Add('undershirts,"undershirt, basic tee, basic t-shirt"');
        big_categories.Add('underwear,"briefs, underwear"');
        big_categories.Add('windbreaker-jackets,windbreaker');

      big_categories.SaveToFile('Mercury_data\categories.dict');
      result:= false;
    end;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  CreateFolder('Mercury_data\Export',true);
  CreateFolder('Mercury_data\Temp',true);

  if not DictionariesInit then
    Log('Problem with dictionaries, some files were missing. Restoring defaults.');
end;



end.
